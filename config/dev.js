const development = {

  title: "Appbricks",

  staticUrl: "",

  languages: ['zh-cn', 'en'],

  // 描述后端是如何匹配URL，以及hbs模板
  routes: {
    '/*': {
      view: 'index',
      scripts: ['vendors', 'index'],
      csses:['vendors']
    }
  },
  isomorphic: false,
  // 描述前端有多少个entry
  entries: {
    'index': `./src/js/app/index.jsx`,
  },
  proxies:[
  {
    api: '/bsl-daq',
    target: 'http://138.68.41.35'
  },
  {
    api: '/:module/api',
    target: 'http://138.68.41.35'
  }
]

}

module.exports = development
