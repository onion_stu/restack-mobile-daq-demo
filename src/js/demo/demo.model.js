import logEvent from 'restack-analyst'

const demo = {
  name: 'demos',
  initialState: {
  },
  sagas: {
    *pathConversion(action,{update, put, call}) {
      console.log(action.payload)

      try{
        yield call(logEvent, `/bsl-daq/api/v1/records/`,
          action.payload.appId, action.payload.tags, action.payload.meta);
      }
      catch(err){
        console.log("log failed");
      }
    },
    *deviceVitality(action,{update, put, call}) {
      console.log(action.payload)
        const response = yield call(fetch, `/bsl-daq/api/v1/records/`,{
        method: 'POST',
        headers: {
        'Content-Type': 'application/json'
        },
        body: JSON.stringify(action.payload)
      })
      if (response) {
        yield put({type: "crudModules.SUCCESS", response});
        // yield put({type: "crudModules.SUCCESS", Object.assign({})})
      } else {
        yield put({type: "crudModules.FAILURE", error})
      }
    },
    *deviceResolution(action,{update, put, call}) {
      console.log(action.payload)
        const response = yield call(fetch, `/bsl-daq/api/v1/records/`,{
        method: 'POST',
        headers: {
        'Content-Type': 'application/json'
        },
        body: JSON.stringify(action.payload)
      })
      if (response) {
        yield put({type: "crudModules.SUCCESS", response});
        // yield put({type: "crudModules.SUCCESS", Object.assign({})})
      } else {
        yield put({type: "crudModules.FAILURE", error})
      }
    },
    *userLocation(action,{update, put, call}) {
      console.log(action.payload)
        const response = yield call(fetch, `/bsl-daq/api/v1/records/`,{
        method: 'POST',
        headers: {
        'Content-Type': 'application/json'
        },
        body: JSON.stringify(action.payload)
      })
      if (response) {
        yield put({type: "crudModules.SUCCESS", response});
        // yield put({type: "crudModules.SUCCESS", Object.assign({})})
      } else {
        yield put({type: "crudModules.FAILURE", error})
      }
    },
     *networkConnection(action,{update, put, call}) {
      console.log(action.payload)
        const response = yield call(fetch, `/bsl-daq/api/v1/records/`,{
        method: 'POST',
        headers: {
        'Content-Type': 'application/json'
        },
        body: JSON.stringify(action.payload)
      })
      if (response) {
        yield put({type: "crudModules.SUCCESS", response});
        // yield put({type: "crudModules.SUCCESS", Object.assign({})})
      } else {
        yield put({type: "crudModules.FAILURE", error})
      }
    },

    *list(action, {update, put, call}) {
      yield update({isFetching: true})

      // yield put({ type: "crudModules.REQUEST", payload: {isFetching: true} })
      const response = yield call(fetch, `/system/api/v1/modules`)
      if (response) {
        yield put({type: "crudModules.SUCCESS", response});
        // yield put({type: "crudModules.SUCCESS", Object.assign({})})
      } else {
        yield put({type: "crudModules.FAILURE", error})
      }
    },
    *update() {

    },
    *fetch() {
      yield console.log("fetching list");
    },
    *destroy() {

    }
  }
}

export default demo
