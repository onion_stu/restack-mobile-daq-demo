import React from 'react'
import {Route, IndexRoute} from 'react-router'

import App from './App'
import Demo from '../demo/DemoPage'
//import Index from '../demo/Index'
//import Next from '../demo/Next'

const rootRoute = (
  <Route path="/" component={App}>
    <IndexRoute component={Demo}/>
  </Route>
)

export default rootRoute
